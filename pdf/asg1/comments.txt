Asg1
-------------------
Grader: Saeed
Overall grade: 73/100
---
Q1 (8/10):
- Fair answer
- Multiple loops does not improve locallity 
- Why is badloc worse? Not explained fully
Q2 (18/20):
- Well plotted
- Fair explained
Q3 (14/15):
- Fair explained
- Well plotted. 
Q4 (15/15):
- Good
Q5 (18/40):
- No comments/Documentation
- No performance tuning 
- No time measurement
Q6 (-/5):
--------------------------------------------------------------------------------------------------------
