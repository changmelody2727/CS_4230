
Grader: Ganesh:
Overall comments:

* Good effort, but in many cases, there could have been 
  some more details provided.
  
Points Awarded: 80

---

Q1: Good answer. Would have been good to see a few more details of the
    equations that these authors provided.
   

Q2: This is a good overview but not a detailed answer because these
    notions are quite important to distinguish clearly.

    
Q3: Your use of units are excellent. However, please note that Cal = KCal
    (so it will last more hours)


Q4: Good observations from Sutter's talk!


Q5: Good summary of acq/rel, but please note that the spelling is
    critical section (not criticle section).

Q6: These are very good examples of programming errors. However, Herb
    Sutter's point is to highlight the reorderings that compilers and
    hardware can make. Could you please listen to Herb Sutter's
    talk where he mentions these reorderings? You will really enjoy
    that part of his talk.

Q7: Excellent. But how about the modern C++ constructs that Chrys
    Woods talks about? Have you already used Lambdas in other classes
    with all the capture-list details?

---

    
