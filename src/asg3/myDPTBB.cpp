#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <tbb/tbb.h>
#include <iostream>

using namespace std;
using namespace tbb;

double *A;
double *B;
double *C;

double probsize = 0;
double numthreads=0;
double tmpSize = 0;

void Parallel_DP(double start, double end);

int main(int argc, char **argv){
    if (argc<2){
        cout << "please provide probsize" << endl;
        exit(EXIT_FAILURE);
    }
    probsize = (double)strtol(argv[1], NULL, 10);
    
    A = (double *) malloc(probsize*sizeof(double));
    B = (double *) malloc(probsize*sizeof(double));
    C = (double *) malloc(probsize*sizeof(double));
    
    for(int i=0; i<probsize; ++i) {
        C[i]=0;
    }
    
    srand(17);
    
    for (int i=0; i<probsize; ++i)
    {
        A[i] = (double) (rand()%16); //printf("A[%d] = %f\n", i, A[i]);
        B[i] = (double) (rand()%16); //printf("B[%d] = %f\n", i, B[i]);
    }
    
    
    for(int nThr = 1; nThr <= 24; nThr++){
        tick_count t0 = tick_count::now();
        task_scheduler_init init(nThr);
        
        tmpSize = probsize/nThr;
        //tick_count t0 = tick_count::now();
        Parallel_DP(0,tmpSize);
        tick_count t1 = tick_count::now();
        
        double sum = 0;
        for(int i = 0; i < probsize; i++){
            sum += C[i];
        }
        
        cout<< "PSize = " << probsize << " nThr = " << nThr << " DP = " << sum << " T = " << (t1-t0).seconds() <<endl;
        
    }
    //
    //    tick_count t0 = tick_count::now();
    //
    //    tick_count t1 = tick_count::now();
    //    cout<< (t1-t0).seconds()  <<endl;
    //cout<< "total time is: " << (t1-t0).seconds()  <<endl;
    //cout<< "result: " << dpsum << endl;
    return 0;
}

void Parallel_DP(double start, double end){
    tbb::task_group g;
    if(end <= probsize){
        // calculate the dotproduct
        //for(int i = start; i < end; i++){
        //  C[i] = A[i] * B[i];
        //}
        if(end < probsize-1)
        {
            // start next section
            g.run([=]{Parallel_DP(end, (end+tmpSize));});
        }
        // calculate the dotproduct
        for(int i = start; i < end; i++){
            C[i] = A[i] * B[i];
        }
    }
    // base case
    g.wait();
}



