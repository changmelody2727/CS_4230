Before using ISP:

1. Make sure that the main carries argc and argv as in

   int main(int argc, char* argv[]) ...

2. The startup of MPI must look like 

   MPI_Init(&argc, &argv);

   and not NULL as the args of MPI_Init

3. The general commands to use:

   a. ispcc prog.c -o prog.exe
   b. isp -n <numproc> -l <a-log-file-name> file.exe <additional-args-to-prog>
   c. ispUI <a-log-file-name>

<Other notes here>
Enjoy!
