#include <stdio.h>
//#include <sys/sysinfo.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>

const long MAX_EXP = 32;
int Exp, Thres;

typedef struct {
    int L;
    int H;
int nthr; //num of threads
int rslt //dot product result
} RNG;

// Global arrays over which DP is done
float *A;
float *B;
float *C;

void Usage(char *prog_name) {
    fprintf(stderr, "usage: %s <Exp>:int <Thres>:int\n", prog_name);
    fprintf(stderr, "Ensure that Thres <= pow(2, Exp)\n");
    exit(0);
}  /* Usage */

void Get_args(int argc, char **argv) {
    if (argc != 3) Usage(argv[0]);
    Exp = strtol(argv[1], NULL, 10);
    if (Exp <= 0 || Exp > MAX_EXP) Usage(argv[0]);
    Thres = strtol(argv[2], NULL, 10);
    if (Thres < 1 || Thres >= (int) pow(2, Exp)) Usage(argv[0]);
}

int serdp(RNG rng) {
	int rslt = 0;
    for(int i=rng.L; i<=rng.H; ++i){
        C[i] = A[i] * B[i];
	rslt += C[i];
	}
	return rslt;
}

void* pdp(RNG *myrng) {
RNG *rngptr = ((RNG *) myrng);
    RNG rng = *rngptr;
    if ((rng.H - rng.L) <= Thres) {
        rngptr->rslt = serdp(rng);
    }
    else {
        printf("-> rng.L and rng.H are %d %d\n", rng.L, rng.H);
        
        RNG rngL = rng;
        RNG rngH = rng;
        
        rngL.H = rng.L + (rng.H - rng.L)/2;
        rngH.L = rngL.H+1;

    	//threads created    
        pthread_t pid1;
        pthread_create(&pid1,NULL,pdp,(void*) &rngL);
        
        pthread_t pid2;
        pthread_create(&pid2,NULL,pdp,(void*) &rngH);
        
        pthread_join(pid1,NULL);
        pthread_join(pid2,NULL);
        
	rngptr->rslt = rngL.rslt+rngH.rslt;
    }
    return NULL;
}

int get_nprocs_conf(void);
int get_nprocs(void);

int main(int argc, char **argv) {
    // Turn this on on Linux systems
    // On Mac, it does not work
    printf("This system has\
           %d processors configured and\
           %d processors available.\n",
           get_nprocs_conf(), get_nprocs());
    
    Get_args(argc, argv);
    int Size = (int) pow(2, Exp);
    printf("Will do DP of %d sized arrays\n", Size);
    
    A = (float *) malloc(Size*sizeof(float));
    B = (float *) malloc(Size*sizeof(float));
    printf("Filling arrays now\n");
    srand(17); //Seed with 17
    for (int i=0; i<Size; ++i) {
        A[i] = rand()%16;
        B[i] = rand()%16;
    }
//    for(int i=0; i<Size; ++i) {
  //      printf("A[%d] and B[%d] are %f and %f\n",i,i,(double)A[i],(double)B[i]);
//    }
    
    RNG rng;
    rng.L = 0;
    rng.H = Size-1;
    //printf("Serial dot product is %f\n", serdp(rng));
    
    printf("Now invoking parallel dot product\n");
    C = (float *) malloc(Size*sizeof(float));
    for(int i=0; i<Size; ++i) {
        C[i]=0;
    }
    
//double start= 0.0;
//  GET_TIME(start);

	printf("Parallel DP = %f\n", pdp(&rng));
  //  pdp(&rng);
//double finish = 0.0;
//  GET_TIME(finish);

//  double elapsed = finish - start;
//printf("The elapsed time is %e seconds\n", elapsed);

	//double FinC;    

    printf("Final C is\n");
	printf("%d\n", rng.rslt);
    //for(int i=0; i<Size; ++i) {
     //   printf("%f\n", (double) C[i]);
        
   // }
    
    free(A);
    free(B);
    free(C);
}
