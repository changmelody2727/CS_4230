chingyc@lab1-1:Primes$ 
chingyc@lab1-1:Primes$ 
chingyc@lab1-1:Primes$ gcc-7.3 -o ipr -fsanitize=thread -static-libtsan iP.c 
iP.c: In function 'worker':
iP.c:63:19: warning: return makes pointer from integer without a cast [-Wint-conversion]
       else return work;
                   ^~~~
iP.c: At top level:
iP.c:67:1: warning: return type defaults to 'int' [-Wimplicit-int]
 main(int argc, char **argv)
 ^~~~
iP.c: In function 'main':
iP.c:70:8: warning: implicit declaration of function 'atoi'; did you mean 'atan'? [-Wimplicit-function-declaration]
    n = atoi(argv[1]);
        ^~~~
        atan
iP.c:86:34: warning: passing argument 3 of 'pthread_create' from incompatible pointer type [-Wincompatible-pointer-types]
       pthread_create(&id[i],NULL,worker,i);
                                  ^~~~~~
In file included from iP.c:16:0:
/usr/include/pthread.h:244:12: note: expected 'void * (*)(void *)' but argument is of type 'void * (*)(int)'
 extern int pthread_create (pthread_t *__restrict __newthread,
            ^~~~~~~~~~~~~~
iP.c:86:41: warning: passing argument 4 of 'pthread_create' makes pointer from integer without a cast [-Wint-conversion]
       pthread_create(&id[i],NULL,worker,i);
                                         ^
In file included from iP.c:16:0:
/usr/include/pthread.h:244:12: note: expected 'void * restrict' but argument is of type 'int'
 extern int pthread_create (pthread_t *__restrict __newthread,
            ^~~~~~~~~~~~~~
iP.c:94:26: warning: passing argument 2 of 'pthread_join' from incompatible pointer type [-Wincompatible-pointer-types]
       pthread_join(id[i],&work);
                          ^
In file included from iP.c:16:0:
/usr/include/pthread.h:261:12: note: expected 'void **' but argument is of type 'int *'
 extern int pthread_join (pthread_t __th, void **__thread_return);
            ^~~~~~~~~~~~




chingyc@lab1-1:Primes$ ./ipr 100000 4 3
==================
WARNING: ThreadSanitizer: data race (pid=2203)
  Write of size 4 at 0x00000149993c by thread T2:
    #0 crossout <null> (ipr+0x00000048c447)
    #1 worker <null> (ipr+0x00000048c54a)

  Previous write of size 4 at 0x00000149993c by thread T1:
    #0 crossout <null> (ipr+0x00000048c447)
    #1 worker <null> (ipr+0x00000048c54a)

  Location is global 'prime' of size 400000004 at 0x000001499900 (ipr+0x00000149993c)

  Thread T2 (tid=2206, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (ipr+0x00000040c7f0)
    #1 main <null> (ipr+0x00000048c6e1)

  Thread T1 (tid=2205, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (ipr+0x00000040c7f0)
    #1 main <null> (ipr+0x00000048c6e1)

SUMMARY: ThreadSanitizer: data race (/home/chingyc/CS_4230/src/pthreads/Primes/ipr+0x48c447) in crossout
==================
==================
WARNING: ThreadSanitizer: data race (pid=2203)
  Write of size 4 at 0x000001499964 by thread T2:
    #0 crossout <null> (ipr+0x00000048c447)
    #1 worker <null> (ipr+0x00000048c54a)

  Previous read of size 4 at 0x000001499964 by thread T1:
    #0 worker <null> (ipr+0x00000048c52b)

  Location is global 'prime' of size 400000004 at 0x000001499900 (ipr+0x000001499964)

  Thread T2 (tid=2206, running) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (ipr+0x00000040c7f0)
    #1 main <null> (ipr+0x00000048c6e1)

  Thread T1 (tid=2205, finished) created by main thread at:
    #0 pthread_create ../../.././libsanitizer/tsan/tsan_interceptors.cc:900 (ipr+0x00000040c7f0)
    #1 main <null> (ipr+0x00000048c6e1)

SUMMARY: ThreadSanitizer: data race (/home/chingyc/CS_4230/src/pthreads/Primes/ipr+0x48c447) in crossout
==================
64 values of base done
1 values of base done
1 values of base done
0 values of base done
0 values of base done
0 values of base done
0 values of base done
0 values of base done
0 values of base done
0 values of base done

the number of primes found was 9592
ThreadSanitizer: reported 2 warnings
