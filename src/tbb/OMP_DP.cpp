#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>
#include <tbb/tbb.h>
#include <iostream>

using namespace std;
using namespace tbb;

double *A;
double *B;
double *C;

double probsize = 0;
double threshold = 0;
double numthreads=0;


int main(int argc, char **argv){
    if (argc<4){
        cout << "please provide probsize, threshold, numthreads" << endl;
        exit(EXIT_FAILURE);
    }
    probsize = (double)strtol(argv[1], NULL, 10);
    threshold = (double)strtol(argv[2], NULL, 10);
    numthreads = (double)strtol(argv[3], NULL, 10);
    
    A = (double *) malloc(probsize*sizeof(double));
    B = (double *) malloc(probsize*sizeof(double));
    C = (double *) malloc(probsize*sizeof(double));
    
    for(int i=0; i<probsize; ++i) {
        C[i]=0;
    }
    
    srand(17);
    
    for (int i=0; i<probsize; ++i)
    {
        A[i] = (double) (rand()%16); //printf("A[%d] = %f\n", i, A[i]);
        B[i] = (double) (rand()%16); //printf("B[%d] = %f\n", i, B[i]);
    }
    
    double dpsum = 0;
    // run the program with multiple thresholds
    for(double thres = threshold; thres <= probsize; thres = thres*2){
        cout << "threshold: " << thres<< endl;
        for( int p=1; p<=numthreads; ++p ) {
            tick_count t0 = tick_count::now();
            for (int j=0; j<16; ++j) { // just to add some bloat
                dpsum = 0;
                //count how many index each thread will need to do DP
                //double base = probsize/threshold;
                //double base = probsize/thres;
                #pragma omp parallel num_threads(p) // thr cnt
                {
                #pragma omp for schedule (dynamic, thres) reduction(+: dpsum)
                    for(int i = 0; i< probsize;i++) {
                           dpsum += A[i] * B[i];
                            //dpsum += C[j];
                        
                    }
                }
            }
            
            tick_count t1 = tick_count::now();
            cout<< "PSize = " << probsize << " nThr = " << p << " DP = " << dpsum << " T = " << (t1-t0).seconds() <<endl;
//            cout<< (t1-t0).seconds()  <<endl;
            //cout<< "total time is: " << (t1-t0).seconds()  <<endl;
            //cout<< "result: " << dpsum << endl;
            
            
        }
    }
    return 0;
}


